# Install WordPress on VM(ubuntu-20.04) and Configure it to Use a Database in another VM(ubuntu-20.04).
### In this repo I will demonstrate how to Install WordPress on VM and and Configure it to Use a Database in another VM.



# Getting started

## A. Configure Database For WordPress.

1. Connect to the vm that you want to install database in it.
2. Oppen port 3306 (MYSQL) **see this https://gitlab.com/ninos77/openstack-cli-create-virtual-server**.
3. Install Mariadb (MYSQL)```sudo apt install mariadb-server```.
4. To improve the security of your MariaDB install mysql_secure_installation ``` sudo mysql_secure_installation ``` .
5. Login your database ``` sudo mysql ```.
6. Create your wordpress database ``` create database wordpress default character set utf8 collate utf8_unicode_ci; ``` .
7. Create user ``` create user 'wordpress'@'IP_address_where_WordPress_will_be_installed' identified by 'PASSWORD';```.
8. Create privileges ``` grant all privileges on wordpress.* to 'wordpress'@'IP_address_where_WordPress_will_be_installed'; ```.
9. Make a flush for your privileges to be valid ```flush privileges;```.
10. Now the database is created type ```exit```.
11. Check if the database it's active by typing ``` sudo systemctl status mariadb.service```.
12. Type ``` sudo ss -tulpn``` to ckeck Local Address:Port for user nysql It is set to listen on localhost only **127.0.0.1:3306**. <br/>
    So To accept remote connections, edit the MySQL configuration file and modify the **bind-address** option. It is set to listen on localhost only. We will change 127.0.0.1 to the database server IP address.
- Navigate to this folder /etc/mysql/mariadb.conf.d and open this file ***50-server.cnf***  ```sudo nano 50-server.cnf```and change **bind-address** from local 127.0.0.1 to the database server IP address.
13. Do Restart for database server ``` sudo systemctl restart mariadb.service ``` to be valid.


## B. Install WordPress in another VM(ubuntu-20.04) and Configure it to Use a Database in another VM(ubuntu-20.04).

### 1. Install Caddy for Web Server  
- Connect to the vm that you want to install Wordpress in it.
- Oppen port 80,443 for web server and TLS or SSL certificate HTTPS. **see this https://gitlab.com/ninos77/openstack-cli-create-virtual-server**.
- To install Caddy follow guidance from this link for more security and if there is any update ----> https://caddyserver.com/docs/install#debian-ubuntu-raspbian.
- To check if Caddy.service is active (running) ``` sudo systemctl status caddy.service ```.

### 2. Install Wordpress
- Navigate to folder www ```cd /var/www ``` if is not found, navigate to folder var ``` cd /var ``` and create folde www ```sudo mkdir www```.
- Change permission for (user and group )for www folder from root to www-data ``` sudo chown -R www-data:www-data www/``` and make cd to www.
- Install wordpress ```sudo wget https://wordpress.org/latest.tar.gz```.
- Extracts the file in the current directory that you have download latest.tar.gz ``` sudo tar -xzvpf latest.tar.gz ```.
- Now you hace Wordpress in this location /var/wwww/wordpress. We have to change the permission for wordpress folder ``` sudo chown -R www-data:www-data wordpress/ ```.

### 3. Install PHP and php-mysql.
- type ``` sudo apt install php php-mysql```.
- We will remove ***apache2*** that has been installed with the installation of php. We do not need to apache2 because we have Caddy ```sudo apt purge apache2* ``` .
- check which version php is and write it down we need it a little later ``` php -v ```.
- Instal php package and library ```sudo apt install php-mysqlnd php-gd php-json php-mbstring php-xml php-imagick php-intl php-dom php-curl php-zip```.

### 4. Configure Caddyfile
- Open your Caddyfile with your edit text in the server ```sudo nano /etc/caddy/Caddyfile```. change it like the one below **You Must Be Very Careful When Writing This File** <br/>
``` # The Caddyfile is an easy way to configure your Caddy web server.
#
# Unless the file starts with a global options block, the first
# uncommented line is always the address of your site.
#
# To use your own domain name (with automatic HTTPS), first make
# sure your domain's A/AAAA DNS records are properly pointed to
# this machine's public IP, then replace ":80" below with your
# domain name.

#:80 {
        # Set this path to your site's directory.
        root * /usr/share/caddy

        # Enable the static file server.
#       file_server

        # Another common task is to set up a reverse proxy:
        # reverse_proxy localhost:8080

        # Or serve a PHP site through php-fpm:
        # php_fastcgi localhost:9000
#}

:443 {
        root * /var/www/wordpress
        php_fastcgi unix//run/php/php7.4-fpm.sock
        file_server
        encode gzip

        @disallowed {
                path /xmlrpc.php
                path *.sql
                path /wp-content/uploads/*.php
        }
        rewrite @disallowed '/index.php'

        tls internal {
                on_demand
        }


}

# Refer to the Caddy docs for more information:
# https://caddyserver.com/docs/caddyfile
``` 
- Restart your Caddy service ``` sudo systemctl restart caddy.service```.
- write your ip address or your domain in internet browser to test if Your WordPress is Live.

